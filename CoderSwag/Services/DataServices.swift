//
//  DataServices.swift
//  CoderSwag
//
//  Created by Marc Michel on 8/26/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation
class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(title: "SHIRTS", ImageName: "shirts.png"),
        Category(title: "HOODIES", ImageName: "hoodies.png"),
        Category(title: "HATS", ImageName: "hats.png"),
        Category(title: "DIGITAL", ImageName: "digital.png")
    
    ]
    
    private let hats = [
        Product(title: "Devslopes Logo Graphic Beanie", price: "$18.00", imageName: "hat01.png"),
        Product(title: "Devslopes Logo Black Hat", price: "$21.00", imageName: "hat02.png"),
        Product(title: "Devslopes Logo White Hat", price: "$17.03", imageName: "hat03.png"),
        Product(title: "Devslopes Logo Snapback", price: "$25.08", imageName: "hat04.png")
    ]
    
    private let hoodies = [
        Product(title: "Devslopes Logo graphic Hoodie", price: "$31.00", imageName: "hoodie01.png"),
        Product(title: "Devslopes Logo Red Hoodie", price: "$35.00", imageName: "hoodie02.png"),
        Product(title: "Devslopes Hoodie Grey", price: "$21.00", imageName: "hoodie03.png"),
        Product(title: "Devslopes Hoodie Black", price: "$11.00", imageName: "hoodie04.png")
    ]
    
    private let shirts = [
        Product(title: "Devslopes Logo Shirt Black", price: "$8.00", imageName: "shirt01.png"),
        Product(title: "Devslopes Badge Shirt Light Grey", price: "$12.00", imageName: "shirt02.png"),
        Product(title: "Devslopes Logo Shirt Red", price: "$14.00", imageName: "shirt03.png"),
        Product(title: "Devslopes Logo Shirt Grey", price: "$5.00", imageName: "shirt04.png"),
        Product(title: "Whatever Logo Shirt Black", price: "$2.00", imageName: "shirt05.png")
    ]
    
    private let digitalGoods = [Product]()
    
    func getCategories() -> [Category] {
        return categories
    }
    
    func getProducts(forCategoryTile title: String)->[Product] {
        switch title {
        case "SHIRTS":
            return getShirt()
        case "HATS":
            return getHat()
        case "HOODIES":
            return getHoodies()
        case "DIGITAL":
            return getDigitalGood()
        default:
            return getShirt()
        }
    }
    
    func getHat() -> [Product] {
        return hats
    }
    
    func getHoodies() -> [Product] {
        return hoodies
    }
    
    func getShirt() -> [Product] {
        return shirts
    }
    
    func getDigitalGood() -> [Product] {
        return digitalGoods
    }
}
