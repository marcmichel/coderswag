//
//  CustomCell.swift
//  CoderSwag
//
//  Created by Marc Michel on 8/26/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func updateView(category: Category) {
        categoryImage.image = UIImage(named: category.ImageName)
        titleLabel.text = category.title
    }


}
