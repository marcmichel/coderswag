//
//  CollectionViewCell.swift
//  CoderSwag
//
//  Created by Marc Michel on 8/26/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel :UILabel!
    @IBOutlet weak var priceLabel : UILabel!
    
    
    func updateView(product : Product) {
        imageView.image = UIImage(named: product.imageName)
        titleLabel.text = product.title
        priceLabel.text = product.price
    }
}
