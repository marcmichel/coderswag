//
//  ProductVCViewController.swift
//  CoderSwag
//
//  Created by Marc Michel on 8/26/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class ProductVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var productCollection: UICollectionView!
    
    
    private(set) public var products = [Product]()

    override func viewDidLoad() {
        super.viewDidLoad()

        productCollection.delegate = self
        productCollection.dataSource = self
    }
    
    func initProduct(category: Category) {
        products = DataService.instance.getProducts(forCategoryTile: category.title)
        navigationItem.title = category.title
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell {
            let product = products[indexPath.row]
            cell.updateView(product: product)
            return cell
        } else {
            return CollectionViewCell()
        }
        
    }

}
