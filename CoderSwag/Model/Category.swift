//
//  Category.swift
//  CoderSwag
//
//  Created by Marc Michel on 8/26/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

struct Category {
    private(set) public var title: String
    private(set) public var ImageName: String
    
    init(title: String, ImageName: String) {
        self.title = title
        self.ImageName = ImageName
    }
}
